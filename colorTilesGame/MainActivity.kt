package com.example.colortilesgame

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import kotlin.random.Random


data class Coord(val x: Int, val y: Int)

class MainActivity : AppCompatActivity() {

    lateinit var tiles: Array<Array<View>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tiles = arrayOf(
            arrayOf(findViewById(R.id.t00), findViewById(R.id.t01), findViewById(R.id.t02), findViewById(R.id.t03)),
            arrayOf(findViewById(R.id.t10), findViewById(R.id.t11), findViewById(R.id.t12), findViewById(R.id.t13)),
            arrayOf(findViewById(R.id.t20), findViewById(R.id.t21), findViewById(R.id.t22), findViewById(R.id.t23)),
            arrayOf(findViewById(R.id.t30), findViewById(R.id.t31), findViewById(R.id.t32), findViewById(R.id.t33))
        )

        initField()
    }

    fun getCoordFromString(s: String): Coord {
        val x = Character.getNumericValue(s[0])
        val y = Character.getNumericValue(s[1])
        return Coord(x, y)
    }

    fun changeColor(view: View) {
        val brightColor = resources.getColor(R.color.bright, null)
        val darkColor = resources.getColor(R.color.dark, null)
        val drawable = view.background as ColorDrawable
        if (drawable.color == brightColor) {
            view.setBackgroundColor(darkColor)
        } else {
            view.setBackgroundColor(brightColor)
        }
    }

    fun onClick(v: View) {
        val coord = getCoordFromString(v.tag.toString())
        changeColor(v)

        for (i in 0..3) {
            changeColor(tiles[coord.x][i])
            changeColor(tiles[i][coord.y])
        }

        checkVictory()
    }

    fun checkVictory() {
        val firstTileColor = (tiles[0][0].background as ColorDrawable).color

        for (i in 0..3) {
            for (j in 0..3) {
                val currentColor = (tiles[i][j].background as ColorDrawable).color
                if (currentColor != firstTileColor) {
                    return
                }
            }
        }

        Toast.makeText(this, "You won!", Toast.LENGTH_SHORT).show()
    }

    fun initField() {
        for (i in 0..3) {
            for (j in 0..3) {
                val randomColor = if (Math.random() < 0.5) {
                    resources.getColor(R.color.bright, null)
                } else {
                    resources.getColor(R.color.dark, null)
                }
                tiles[i][j].setBackgroundColor(randomColor)
            }
        }
    }
}