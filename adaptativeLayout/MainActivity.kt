package com.example.layoutadapt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import android.widget.Toast

class MainActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener {
    lateinit var adapter: ArrayAdapter<CharSequence>
    lateinit var picturesList: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = ArrayAdapter.createFromResource(this, R.array.pictures, R.layout.item)
        picturesList = findViewById<Spinner>(R.id.pictures_list) ?: Spinner(this)
        picturesList.adapter = adapter
        picturesList.onItemSelectedListener = this
    }

    fun onChangePictureClick(v: View) {
        val iv = findViewById<ImageView>(R.id.picture)
        val pictures = resources.getStringArray(R.array.pictures)

        val currentPosition = picturesList.selectedItemPosition
        val nextPosition = (currentPosition + 1) % pictures.size

        when (pictures[nextPosition]) {
            getString(R.string.car1) -> iv.setImageResource(R.drawable.car1)
            getString(R.string.car2) -> iv.setImageResource(R.drawable.car2)
            getString(R.string.car3) -> iv.setImageResource(R.drawable.car3)
            else -> iv.setImageResource(R.drawable.squarecat)
        }

        picturesList.setSelection(nextPosition)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val iv = findViewById<ImageView>(R.id.picture)
        val pictures = resources.getStringArray(R.array.pictures)

        when (pictures[position]) {
            getString(R.string.car1) -> iv.setImageResource(R.drawable.car1)
            getString(R.string.car2) -> iv.setImageResource(R.drawable.car2)
            getString(R.string.car3) -> iv.setImageResource(R.drawable.car3)
            else -> iv.setImageResource(R.drawable.squarecat)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        Toast.makeText(this, "не выбран элемент", Toast.LENGTH_SHORT ).show()
    }


}