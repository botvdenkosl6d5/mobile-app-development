package com.example.guessnumber

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class GuessActivity : AppCompatActivity() {

    private lateinit var guessTextView: TextView
    private lateinit var higherButton: Button
    private lateinit var lowerButton: Button
    private lateinit var correctButton: Button
    private lateinit var restartButton: Button
    private var low: Int = 0
    private var high: Int = 0
    private var guess: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guess)

        guessTextView = findViewById(R.id.guessTextView)
        higherButton = findViewById(R.id.higherButton)
        lowerButton = findViewById(R.id.lowerButton)
        correctButton = findViewById(R.id.correctButton)
        restartButton = findViewById(R.id.restartButton)

        low = intent.getIntExtra("from", 0)
        high = intent.getIntExtra("to", 0)

        makeGuess()

        higherButton.setOnClickListener {
            low = guess + 1
            makeGuess()
        }

        lowerButton.setOnClickListener {
            high = guess - 1
            makeGuess()
        }

        correctButton.setOnClickListener {
            showResult(true)
        }

        restartButton.setOnClickListener {
            restartGame()
        }
    }

    private fun makeGuess() {
        if (low <= high) {
            guess = (low + high) / 2
            guessTextView.text = getString(R.string.guess_message, guess)
            updateButtonVisibility(true)
        } else {
            showResult(false)
        }
    }

    private fun showResult(correct: Boolean) {
        if (correct) {
            guessTextView.text = getString(R.string.correct_guess_message)
        } else {
            guessTextView.text = getString(R.string.out_of_range_message)
        }
        updateButtonVisibility(false)
    }

    private fun restartGame() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish() // Закрыть текущую активность GuessActivity
    }

    private fun updateButtonVisibility(inRange: Boolean) {
        higherButton.visibility = if (inRange) View.VISIBLE else View.GONE
        lowerButton.visibility = if (inRange) View.VISIBLE else View.GONE
        correctButton.visibility = if (inRange) View.VISIBLE else View.GONE
        restartButton.visibility = if (inRange) View.GONE else View.VISIBLE
    }
}

