package com.example.guessnumber

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val startButton: Button = findViewById(R.id.startButton)
        val fromEditText: EditText = findViewById(R.id.fromEditText)
        val toEditText: EditText = findViewById(R.id.toEditText)

        startButton.setOnClickListener {
            val from = fromEditText.text.toString()
            val to = toEditText.text.toString()

            if (from.isNotEmpty() && to.isNotEmpty()) {
                val intent = Intent(this, GuessActivity::class.java)
                intent.putExtra("from", from.toInt())
                intent.putExtra("to", to.toInt())
                startActivity(intent)
            }
        }
    }
}
