package com.example.myapplication

import android.app.DatePickerDialog
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val editTextDepartureDate: EditText = findViewById(R.id.editTextDepartureDate)
        val editTextReturnDate: EditText = findViewById(R.id.editTextReturnDate)
        val buttonSearch: Button = findViewById(R.id.buttonSearch)

        editTextDepartureDate.setOnClickListener {
            showDatePickerDialog(editTextDepartureDate)
        }

        editTextReturnDate.setOnClickListener {
            showDatePickerDialog(editTextReturnDate)
        }

        buttonSearch.setOnClickListener {
            // Just button Listener
        }
    }

    private fun showDatePickerDialog(editText: EditText) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            { _, selectedYear, selectedMonth, selectedDay ->
                editText.setText("$selectedDay.${selectedMonth + 1}.$selectedYear")
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }
}
