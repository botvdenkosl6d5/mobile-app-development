package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.os.Bundle;
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var moviesList: ArrayList<String>
    private lateinit var displayTextView: TextView
    private lateinit var showMovieButton: Button
    private lateinit var watchedButton: Button
    private lateinit var resetButton: Button
    private var watchedMovies: MutableSet<String> = mutableSetOf()

    private var noMoviesAdded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        moviesList = ArrayList(resources.getStringArray(R.array.moviesArray).toList())

        displayTextView = findViewById(R.id.displayTextView)
        showMovieButton = findViewById(R.id.showMovieButton)
        watchedButton = findViewById(R.id.watchedButton)
        resetButton = findViewById(R.id.resetButton)

        showMovieButton.setOnClickListener { showRandomMovie() }
        watchedButton.setOnClickListener { markAsWatched() }
        resetButton.setOnClickListener { resetMoviesList() }

        showRandomMovie()
    }

    private fun showRandomMovie() {
        if (moviesList.isEmpty()) {
            displayTextView.text = resources.getString(R.string.no_movies_left)
        } else {
            val randomIndex = Random().nextInt(moviesList.size)
            val randomMovie = moviesList[randomIndex]
            displayTextView.text = randomMovie
        }
    }

    private fun markAsWatched() {
        val currentMovie = displayTextView.text.toString()
        if (currentMovie.isNotBlank() && currentMovie != resources.getString(R.string.no_movies_left)) {
            watchedMovies.add(currentMovie)
            moviesList.remove(currentMovie)
            showRandomMovie()
        }
    }
    private fun resetMoviesList() {
        moviesList.addAll(watchedMovies)
        watchedMovies.clear()
        showRandomMovie()
    }
}