package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.text.Html
import java.io.IOException
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.InputStreamReader
import java.lang.reflect.Type
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var movies: List<Movie>
    private lateinit var displayTextView: TextView
    private lateinit var showMovieButton: Button
    private lateinit var watchedButton: Button
    private lateinit var resetButton: Button
    private var watchedMovies: MutableSet<Movie> = mutableSetOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        displayTextView = findViewById(R.id.displayTextView)
        showMovieButton = findViewById(R.id.showMovieButton)
        watchedButton = findViewById(R.id.watchedButton)
        resetButton = findViewById(R.id.resetButton)

        loadMovies()

        showMovieButton.setOnClickListener { showRandomMovie() }
        watchedButton.setOnClickListener { markAsWatched() }
        resetButton.setOnClickListener { resetMoviesList() }

        showRandomMovie()
    }

    private fun loadMovies() {
        val moviesStream = resources.openRawResource(R.raw.movies)
        try {
            val gson = Gson()
            val type: Type = object : TypeToken<Movies>() {}.type
            val moviesData: Movies = gson.fromJson(InputStreamReader(moviesStream), type)
            movies = moviesData.movies.toList()
        } finally {
            try {
                moviesStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    private fun showRandomMovie() {
        if (movies.isEmpty()) {
            displayTextView.text = resources.getString(R.string.no_movies_left)
        } else {
            val remainingMovies = movies.filterNot { watchedMovies.contains(it) }
            if (remainingMovies.isEmpty()) {
                displayTextView.text = resources.getString(R.string.no_movies_left)
            } else {
                val randomIndex = Random().nextInt(remainingMovies.size)
                val randomMovie = remainingMovies[randomIndex]
                displayTextView.text = formatMovieInfo(randomMovie)
            }
        }
    }

    private fun formatMovieInfo(movie: Movie): CharSequence {
        val movieInfo = """
            <b>Название:</b> ${movie.name}<br>
            <b>Год:</b> ${movie.year}<br>
            <b>Рейтинг:</b> ${movie.rating}<br>
            <b>Жанры:</b> ${movie.genres.joinToString(", ")}<br>
            <b>Режиссер:</b> ${movie.director}
        """.trimIndent()

        return Html.fromHtml(movieInfo, Html.FROM_HTML_MODE_LEGACY)
    }

    private fun markAsWatched() {
        Log.d("MainActivity", "MarkAsWatched method called")
        val currentMovie = displayTextView.text.toString()
        Log.d("MainActivity", "Current movie: $currentMovie")
        if (currentMovie.isNotBlank() && currentMovie != resources.getString(R.string.no_movies_left)) {
            val movie = findMovieByName(currentMovie)
            if (movie != null) {
                Log.d("MainActivity", "Movie found: $movie")
                watchedMovies.add(movie)
                movies = movies.filter { it != movie }
                Log.d("MainActivity", "Marked as watched: $currentMovie")
                showRandomMovie()
            } else {
                Log.d("MainActivity", "Movie not found")
            }
        } else {
            Log.d("MainActivity", "Invalid current movie")
        }
    }



    private fun resetMoviesList() {
        movies = movies + watchedMovies
        watchedMovies.clear()
        showRandomMovie()
    }


    private fun findMovieByName(name: String): Movie? {
        return movies.find { formatMovieInfo(it).toString() == name }
    }

}
