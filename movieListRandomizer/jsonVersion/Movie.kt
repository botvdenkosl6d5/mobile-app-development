package com.example.myapplication

data class Movie (val name: String, val year: Int, val rating: Float, val genres: Array<String>, val director: String) {

}